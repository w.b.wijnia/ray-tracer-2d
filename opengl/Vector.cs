﻿using System;

namespace Template
{
    struct Vector
    {
        public float x, y;

        public Vector(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector Rotated(float radians)
        {
            return new Vector(
                (float)(x * Math.Cos(radians) - y * Math.Sin(radians)),
                (float)(x * Math.Sin(radians) + y * Math.Cos(radians))
                );
        }
    }
}
