﻿
using System.Runtime.InteropServices;
using OpenTK;

namespace Template
{
    [StructLayout(LayoutKind.Sequential, Size = 32)]
    public struct Camera
    {

        // the origin of the camera
        public Vector2 origin;

        // the dimensions of the camera
        public Vector2 dimensions;

        public Camera(Vector2 origin, Vector2 dimensions)
        {
            this.origin = origin;
            this.dimensions = dimensions;
        }

        public override string ToString()
        {
            return $"Camera(origin: {origin.ToString()}, dimensions: {dimensions.ToString()},)";
        }
    }
}
