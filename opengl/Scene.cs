﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Template
{
    public class Scene
    {

        public List<Circle> circles;
        public List<Light> lights;

        public Scene()
        {
            lights = new List<Light>();
            circles = new List<Circle>();
        }

        public static Scene PresetA()
        {
            Scene scene = new Scene();

            // rng used for this preset
            Random rng = new Random(8);

            // more realistic colors
            Vector4[] colorsCircles =
            {
                new Vector4(1.0f, 1.0f, 1.0f, 0),
                new Vector4(1.0f, 0.5f, 0.5f, 0),
                new Vector4(0.5f, 1.0f, 1.0f, 0),
                new Vector4(0.5f, 1.0f, 0.5f, 0),
                new Vector4(1.0f, 0.5f, 1.0f, 0),
                new Vector4(0.5f, 0.5f, 0.5f, 0),
                new Vector4(1.0f, 0.0f, 0.0f, 0),
            };

            Vector4[] colorsLights =
            {
                new Vector4(1.0f, 0.8f, 0.8f, 0.0f),
                new Vector4(0.8f, 1.0f, 1.0f, 0.0f),
                new Vector4(0.8f, 0.8f, 1.0f, 0.0f),
            };

            // debugging colors
            //Vector4[] colors =
            //{
            //    new Vector4(1.0f, 1.0f, 1.0f, 0),
            //    new Vector4(1.0f, 0.0f, 0.0f, 0),
            //    new Vector4(0.0f, 0.0f, 1.0f, 0),
            //    new Vector4(0.0f, 1.0f, 0.0f, 0),
            //};

            // generate a bunch of circles
            for (int j = 0; j < 5; j++)
            {
                float x = (float)rng.NextDouble() - 0.5f;
                float y = (float)rng.NextDouble() - 0.5f;
                float offset = 9 * ((float)rng.NextDouble() + 0.1f);
                Vector2 origin = offset * new Vector2(x, y).Normalized();
                float radius = 1 * ((float)rng.NextDouble() + 0.1f);
                Vector4 color = colorsCircles[rng.Next(0, colorsCircles.Length)];

                scene.circles.Add(new Circle(origin, radius, color));
            }

            // generate a bunch of (point) lights
            for (int j = 0; j < 5; j++)
            {
                float x = (float)rng.NextDouble() - 0.5f;
                float y = (float)rng.NextDouble() - 0.5f;
                float offset = 9 * ((float)rng.NextDouble() + 0.1f);
                Vector2 origin = offset * new Vector2(x, y).Normalized();
                float intensity = 0.5f * ((float)rng.NextDouble() + 0.5f);
                Vector4 color = colorsLights[rng.Next(0, colorsLights.Length)];

                scene.lights.Add(new Light(origin, intensity, color));
            }

            return scene;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Scene\r\n");

            sb.Append("\tCircles\r\n");
            foreach (Circle circle in circles)
                sb.Append("\t\t" + circle.ToString() + "\r\n");

            sb.Append("\tLights\r\n");
            foreach (Light light in lights)
                sb.Append("\t\t" + light.ToString() + "\r\n");

            return sb.ToString();
        }
    }
}
