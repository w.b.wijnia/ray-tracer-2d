﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

// The template provides you with a window which displays a 'linear frame buffer', i.e.
// a 1D array of pixels that represents the graphical contents of the window.

// Under the hood, this array is encapsulated in a 'Surface' object, and copied once per
// frame to an OpenGL texture, which is then used to texture 2 triangles that exactly
// cover the window. This is all handled automatically by the template code.

// Before drawing the two triangles, the template calls the Tick method in MyApplication,
// in which you are expected to modify the contents of the linear frame buffer.

// After (or instead of) rendering the triangles you can add your own OpenGL code.

// We will use both the pure pixel rendering as well as straight OpenGL code in the
// tutorial. After the tutorial you can throw away this template code, or modify it at
// will, or maybe it simply suits your needs.

// About using Shader Storage Block Bindings
// https://www.geeks3d.com/20140704/tutorial-introduction-to-opengl-4-3-shader-storage-buffers-objects-ssbo-demo/

// Book of shaders, great resource on GLSL
// https://thebookofshaders.com/

// Common functions
// https://glm.g-truc.net/0.9.4/api/a00129.html

// bits / bytes per type
// https://www.khronos.org/opengl/wiki/OpenGL_Type

// about vec3 and mat3 (use vec4!)
// https://community.khronos.org/t/simple-features-im-missing-in-glsl-enums-and-sizeof/104947

// About useful hotkeys :)
// https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf

namespace Template
{
    public class OpenTKApp : GameWindow
    {
        static bool terminated = false; // application terminates gracefully when this is true

        // dimensions of the screen
        int width = 640;
        int height = 400;

        // determines how much we can see
        float viewDistance = 30;

        // the scene that we're using
        Scene scene;

        // the camera that determines where we view the scene
        Camera camera;

        // the program
        Shaderpack pack;

        // locations of where to store the corresponding input on the GPU
        int locationCameraOrigin, locationCameraDimensions;
        int locationWidth, locationHeight;
        int locationNumberOfLights, locationNumberOfCircles;
        const int bindingLights = 0, bindingCircles = 1;
        int blockLights, blockCircles;
        int bufferLights, bufferCircles;

        // camera controls
        bool trackMouse;
        int prevX = 0;
        int prevY = 0;

        protected override void OnLoad(EventArgs e)
        {
            // prepare our client
            ClientSize = new Size(width, height);

            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            // load in the scene and construct some camera
            scene = Scene.PresetA();
            Console.WriteLine(scene.ToString());
            camera = new Camera(new Vector2(0, 0), new Vector2(viewDistance, viewDistance * ((float)height) / width));

            // load in the (shader) program
            pack = new Shaderpack(
                "shaders/basic-vertex.glsl",
                "shaders/basic-fragment.glsl"
                );

            // determine memory mappings
            locationCameraOrigin = pack.Uniform("cameraOrigin");
            locationCameraDimensions = pack.Uniform("cameraDimensions");
            locationWidth = pack.Uniform("width");
            locationHeight = pack.Uniform("height");
            locationNumberOfLights = pack.Uniform("numberOfLights");
            locationNumberOfCircles = pack.Uniform("numberOfCircles");

            blockLights = pack.Block("lights");
            blockCircles = pack.Block("circles");

            bufferLights = GL.GenBuffer();
            bufferCircles = GL.GenBuffer();
        }

        protected override void OnUnload(EventArgs e)
        {
            // called upon app close
            Environment.Exit(0);      // bypass wait for key on CTRL-F5
        }

        protected override void OnResize(EventArgs e)
        {
            // called upon window resize. Note: does not change the size of the pixel buffer.
            GL.Viewport(0, 0, Width, Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            // get information on input
            var mouse = OpenTK.Input.Mouse.GetState();
            var keyboard = OpenTK.Input.Keyboard.GetState();

            // check if we want to exit
            if (keyboard[OpenTK.Input.Key.Escape]) terminated = true;

            // camera controls
            if (mouse.IsButtonDown(OpenTK.Input.MouseButton.Left))
            {
                if (trackMouse)
                {
                    // compute the proper direction and distance to move
                    Vector2 prev = new Vector2(prevX, -prevY); 
                    Vector2 curr = new Vector2(mouse.X, -mouse.Y);
                    Vector2 dir = prev - curr;

                    // scale it up and translate
                    camera.origin += 0.02f * dir;
                }

                trackMouse = true;
                prevX = mouse.X;
                prevY = mouse.Y;
            }

            if (mouse.IsButtonUp(OpenTK.Input.MouseButton.Left))
                trackMouse = false;

            // animate it a bit
            float angle = 0.01f;
            int circles = scene.circles.Count;
            for (int j = 0; j < circles; j++)
            {
                Circle circle = scene.circles[j];
                Vector2 origin = circle.origin;

                // pre-compute the radians
                float sine = (float)System.Math.Sin(angle);
                float cosine = (float)System.Math.Cos(angle);

                circle.origin = new Vector2(
                    origin.X * cosine - origin.Y * sine,
                    origin.X * sine + origin.Y * cosine
                    );

                scene.circles[j] = circle;
            }
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            // check if we want to quit
            if (terminated)
            {
                Exit();
                return;
            }

            // clear everything out!
            GL.ClearColor(Color.Black);
            GL.Enable(EnableCap.Texture2D);
            GL.Disable(EnableCap.DepthTest);
            GL.Color3(1.0f, 1.0f, 1.0f);

            pack.Use();

            // send data to the GPU
            GL.Uniform1(locationWidth, (float)width);
            GL.Uniform1(locationHeight, (float)height);
            GL.Uniform2(locationCameraOrigin, camera.origin);
            GL.Uniform2(locationCameraDimensions, camera.dimensions);

            // -- PASS CIRCLES -- \\

            // retrieve the circles and determine the number of bytes for a single circle
            Circle[] circles = scene.circles.ToArray();
            int bytesCircle = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Circle));

            // pass the amount of entities
            GL.Uniform1(locationNumberOfCircles, circles.Length);

            // pass the data
            GL.BindBuffer(BufferTarget.ShaderStorageBuffer, bufferCircles);
            GL.BufferData(BufferTarget.ShaderStorageBuffer, circles.Length * bytesCircle, circles, BufferUsageHint.DynamicDraw);
            GL.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, bindingCircles, bufferCircles);
            GL.ShaderStorageBlockBinding(pack.Identifier, blockCircles, bindingCircles);
            GL.BindBuffer(BufferTarget.ShaderStorageBuffer, 0);

            // -- PASS LIGHTS -- \\

            // retrieve the circles and determine the number of bytes for a single circle
            Light[] lights = scene.lights.ToArray();
            int bytesLight = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Light));

            // pass the amount of entities
            GL.Uniform1(locationNumberOfLights, lights.Length);

            // pass the data
            GL.BindBuffer(BufferTarget.ShaderStorageBuffer, bufferLights);
            GL.BufferData(BufferTarget.ShaderStorageBuffer, lights.Length * bytesLight, lights, BufferUsageHint.DynamicDraw);
            GL.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, bindingLights, bufferLights);
            GL.ShaderStorageBlockBinding(pack.Identifier, blockLights, bindingLights);
            GL.BindBuffer(BufferTarget.ShaderStorageBuffer, 0);

            // draw screen filling quad, quad will be colored through the fragment shader
            GL.Begin(PrimitiveType.Quads);
            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(-1.0f, -1.0f);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(1.0f, -1.0f);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(1.0f, 1.0f);
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(-1.0f, 1.0f);
            GL.End();

            // tell OpenTK we're done rendering
            SwapBuffers();
        }
        public static void Main(string[] args)
        {
            // entry point
            using (OpenTKApp app = new OpenTKApp()) { app.Run(30.0, 0.0); }
        }
    }
}