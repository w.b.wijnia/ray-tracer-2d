﻿#version 430

// default definitions of the OpenGL specification
// https://www.khronos.org/opengl/wiki/Fragment_Shader#:~:text=A%20Fragment%20Shader%20is%20the,a%20%22fragment%22%20is%20generated.
in vec4 gl_FragCoord;
in vec2 gl_PointCoord;

// the screen resolution
uniform float width;
uniform float height;

// the camera origin and dimensions of the view box
uniform vec2 cameraOrigin;
uniform vec2 cameraDimensions; // (width / height)

// define the structure of a light, take note of 
// the padding
struct Light
{
    vec4 color;             // 16 bytes
    vec2 origin;            // 24 bytes
    float intensity;        // 28 bytes
    float padding1;         // 32 bytes
};

// all the data surrounding lights
uniform int numberOfLights;
layout(std430,binding = 0) buffer lights {
	Light bufferLights[];
};

// define the structure of a circle, take note of 
// the padding
struct Circle
{
    vec4 color;             // 16 bytes
    vec2 origin;            // 24 bytes
    float radius;           // 28 bytes
    float padding1;         // 32 bytes
};

// all the data surrounding circles
uniform int numberOfCircles;
layout(std430, binding = 1) buffer circles {
	Circle bufferCircles[];
};

// define the structure of a ray, take note no
// padding is required because this struct is
// not synced with the host (the C# code, the CPU)
struct Ray 
{
    vec2 origin;            // 8 bytes
    vec2 direction;         // 16 bytes
    float t;                // 20 bytes
};


// the output of the fragment shader
out vec4 color;

// determines if the given point is inside the given circle
bool insideCircle(Circle circle, vec2 point) {
    vec2 direction = (circle.origin - point);
    float distanceSquared = dot(direction, direction);
    return distanceSquared < circle.radius * circle.radius;
}

// determines if the given ray intersects the given circle
bool intersectsCircle(Circle circle, inout Ray ray) {

    // Step 0: Work everything out on paper!

    // Step 1: Gather all the relevant data.
    float ox = ray.origin.x;
    float dx = ray.direction.x;

    float oy = ray.origin.y;
    float dy = ray.direction.y;

    float x0 = circle.origin.x;
    float y0 = circle.origin.y;
    float cr = circle.radius;

    // Step 2: compute the substitutions.
    float p = ox - x0;
    float q = oy - y0;

    float r = 2 * p * dx;
    float s = 2 * q * dy;

    // Step 3: compute the substitutions, check if there is a collision.
    float a = dx * dx + dy * dy;
    float b = r + s;
    float c = p * p + q * q - cr * cr;

    float DSqrt = b * b - 4 * a * c;

    // no collision possible!
    if (DSqrt < 0)
    {
        return false;
    }

    // Step 4: compute the substitutions.
    float D = sqrt(DSqrt);

    //float t0 = (-b + D) / (2 * a);
    float t1 = (-b - D) / (2 * a);

    if (t1 > 0 && t1 < ray.t)
    {
        ray.t = t1;
        return true;
    }

    return false;
}


void main()
{
    // debugging: is the padding correct?
    // int index = 1;
    // color = vec4(bufferCircles[index].color.x, bufferCircles[index].color.y, bufferCircles[index].color.z, 0);

    // determine ray origin
    vec2 pixel = vec2(gl_FragCoord.x / width, gl_FragCoord.y / height); // -> {0, 1}
    vec2 origin = cameraOrigin + (pixel - 0.5) * cameraDimensions;

    // determine background color
    vec4 background = vec4(1, 1, 1, 1);
    for(int j = 0; j < numberOfCircles; j++)
    {
        Circle circle = bufferCircles[j];
        if(insideCircle(circle, origin)) 
        {
            background = circle.color;
        }
    }

    // determine light color
    vec4 additive = vec4(0, 0, 0, 0);

    // for every light...
    for (int l = 0; l < numberOfLights; l++)
    {
        // retrieve the light
        Light light = bufferLights[l];

        // compute generic data that we need
        vec2 direction = light.origin - origin;
        float magnitude = length(direction);

        // construct the ray
        Ray ray;
        ray.origin = origin;
        ray.direction = normalize(direction);
        ray.t = magnitude;

        // for every circle...
        bool occluded = false;
        for(int c = 0; c < numberOfCircles; c++)
        {
            // determine if the pixel is occluded by the circle
            Circle circle = bufferCircles[c];
            occluded = occluded || intersectsCircle(circle, ray);
        }

        float attenuation = 1.0 / magnitude;
        // if we are occluded, determine a new attenuation
        if (occluded) {
            attenuation = 0;
            // attenuation = attenuation * (ray.t / magnitude) * (ray.t / magnitude);
        }

        additive += attenuation * light.intensity * light.color;

    }

    color = additive * background;
}