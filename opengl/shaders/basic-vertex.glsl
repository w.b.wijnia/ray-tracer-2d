﻿#version 430

// Super duper basic vertex shader: just pass it along!

in layout(location = 0) vec4 position;

void main() {
	gl_Position = position;
}