﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;

using System;
using System.IO;

namespace Template
{
    class Shaderpack
    {
        int program = -1;
        int fragment = -1;
        int vertex = -1;

        public Shaderpack(string pathToVertex, string pathToFragment)
        {
            program = GL.CreateProgram();
            vertex = LoadShader(pathToVertex, ShaderType.VertexShader, program);
            fragment = LoadShader(pathToFragment, ShaderType.FragmentShader, program);
            GL.LinkProgram(program);
        }

        private int LoadShader(string path, ShaderType type, int program)
        {
            int id = GL.CreateShader(type);

            // read the entire source file
            using (StreamReader sr = new StreamReader(path))
            {
                string source = sr.ReadToEnd();
                GL.ShaderSource(id, source);
            }

            // compile and attach
            GL.CompileShader(id);
            GL.AttachShader(program, id);

            Console.WriteLine("Found an error during loading: " + path + " of type " + type.ToString());
            Console.WriteLine(GL.GetShaderInfoLog(id));

            return id;
        }

        /// <summary>
        /// Links the program.
        /// </summary>
        public void Use()
        { GL.UseProgram(program); }

        /// <summary>
        /// Returns an attribute location of the program.
        /// </summary>
        public int Attribute(string name)
        {  return GL.GetAttribLocation(program, name); }


        /// <summary>
        /// Returns an uniform location of the program.
        /// </summary>
        public int Uniform(string name)
        { return GL.GetUniformLocation(program, name); }

        /// <summary>
        /// Returns an block location of the program.
        /// </summary>
        public int Block(string name, ProgramInterface type = ProgramInterface.ShaderStorageBlock)
        { return GL.GetProgramResourceIndex(program, type, name); }

        public int Identifier
        {
            get { return program; }
        }
    }
}
