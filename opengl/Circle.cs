﻿
using System.Runtime.InteropServices;
using OpenTK;

namespace Template
{
    // Understanding the (struct) layout is critical when 
    // communicating with other devices or other platforms.

    // For example, a Vector3 is 12 bytes in C# while in 
    // GLSL it has the same size as a vec4: 16 bytes. This
    // causing the alignment of your data to get messed up.

    // Typically you can assume that anything, any struct
    // and type, has the size of some power of two. Including
    // members of a struct (vec3 in GLSL is 16 bytes).

    [StructLayout(LayoutKind.Sequential, Size = 32)]
    public struct Circle
    {
        // the color of the circle (16 bytes)
        public Vector4 color;

        // the origin of the circle (8 bytes)
        public Vector2 origin;

        // the radius of the circle (4 bytes)
        public float radius;

        public Circle(Vector2 origin, float radius, Vector4 color)
        {
            this.origin = origin;
            this.radius = radius;
            this.color = color;
        }

        public override string ToString()
        {
            return $"Circle(color: {color.ToString()}, origin: {origin.ToString()}, radius: {radius.ToString("0.00")})";
        }
    }
}
