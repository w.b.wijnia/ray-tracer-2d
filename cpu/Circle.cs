﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Template
{
    class Circle
    {
        public Vector2 origin;
        public float radius;
        public Vector3 color;

        public Circle(Vector2 origin, float radius, Vector3 color)
        {
            this.origin = origin;
            this.radius = radius;
            this.color = color;
        }

        // Computes a point on the perimeter of the circle.
        public Vector2 PointOnPerimeter(Random rng)
        {
            // some radian between {0, 2*pi}
            float r = (float)(rng.NextDouble() * (2 * Math.PI));

            // compute the x and y coordinates on the unit circle
            float x = (float)Math.Cos(r);
            float y = (float)Math.Sin(r);

            // compute the final value on the perimeter
            Vector2 direction = new Vector2(x, y);
            return origin + radius * direction;
        }

        // Determines whether a point is inside the circle.
        public bool IsInside(Vector2 point)
        {
            Vector2 direction = point - origin;
            float distance = direction.Length;
            return distance < radius;
        }
    }
}
