﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Template
{
    class Camera
    {
        public Vector2 origin;
        public Vector2 dimensions;

        public Camera(Vector2 origin, Vector2 dimensions)
        {
            this.origin = origin;
            this.dimensions = dimensions;
        }
    }
}
