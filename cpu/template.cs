﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

// The template provides you with a window which displays a 'linear frame buffer', i.e.
// a 1D array of pixels that represents the graphical contents of the window.

// Under the hood, this array is encapsulated in a 'Surface' object, and copied once per
// frame to an OpenGL texture, which is then used to texture 2 triangles that exactly
// cover the window. This is all handled automatically by the template code.

// Before drawing the two triangles, the template calls the Tick method in MyApplication,
// in which you are expected to modify the contents of the linear frame buffer.

// After (or instead of) rendering the triangles you can add your own OpenGL code.

// We will use both the pure pixel rendering as well as straight OpenGL code in the
// tutorial. After the tutorial you can throw away this template code, or modify it at
// will, or maybe it simply suits your needs.

namespace Template
{
	public class OpenTKApp : GameWindow
	{
		int width = 640;
		int height = 400;
		int offset = 1;

		static int screenID;            // unique integer identifier of the OpenGL texture
		public Surface screen;
		static bool terminated = false; // application terminates gracefully when this is true

		Render render;
		Scene scene;
		Camera camera;

		// camera controls
		bool trackMouse;
		int prevX = 0;
		int prevY = 0;

		protected override void OnLoad( EventArgs e )
		{
			// called during application initialization
			GL.ClearColor( 0, 0, 0, 0 );
			GL.Enable( EnableCap.Texture2D );
			GL.Disable( EnableCap.DepthTest );
			GL.Hint( HintTarget.PerspectiveCorrectionHint, HintMode.Nicest );

			ClientSize = new Size(width, height);
			screen = new Surface(width >> offset, height >> offset);

			render = new Render();
			scene = Scene.PresetA();

			// TODO: aspect ratio
			camera = new Camera(Vector2.Zero, new Vector2(30, 30 * ((float)height / width)));

			Sprite.target = screen;
			screenID = screen.GenTexture();
		}

		protected override void OnUnload( EventArgs e )
		{
			// called upon app close
			GL.DeleteTextures( 1, ref screenID );
			Environment.Exit( 0 );      // bypass wait for key on CTRL-F5
		}

		protected override void OnResize( EventArgs e )
		{
			// called upon window resize. Note: does not change the size of the pixel buffer.
			GL.Viewport( 0, 0, Width, Height );
			GL.MatrixMode( MatrixMode.Projection );
			GL.LoadIdentity();
			GL.Ortho( -1.0, 1.0, -1.0, 1.0, 0.0, 4.0 );
		}

		protected override void OnUpdateFrame( FrameEventArgs e )
		{
			// get information on input
			var mouse = OpenTK.Input.Mouse.GetState();
			var keyboard = OpenTK.Input.Keyboard.GetState();

			// check if we want to exit
			if (keyboard[OpenTK.Input.Key.Escape]) terminated = true;

			// camera controls
			if (mouse.IsButtonDown(OpenTK.Input.MouseButton.Left))
			{
				if (trackMouse)
				{
					// compute the proper direction and distance to move
					Vector2 prev = new Vector2(prevX, prevY);
					Vector2 curr = new Vector2(mouse.X, mouse.Y);
					Vector2 dir = prev - curr;

					// scale it up (down) and translate
					camera.origin += 0.02f * dir;
				}

				trackMouse = true;
				prevX = mouse.X;
				prevY = mouse.Y;
			}

			if (mouse.IsButtonUp(OpenTK.Input.MouseButton.Left))
				trackMouse = false;

			// animate it a bit
			float angle = 0.01f;
			int circles = scene.circles.Count;
			for (int j = 0; j < circles; j++)
			{
				Circle circle = scene.circles[j];
				Vector2 origin = circle.origin;

				// pre-compute the radians
				float sine = (float)System.Math.Sin(angle);
				float cosine = (float)System.Math.Cos(angle);

				circle.origin = new Vector2(
					origin.X * cosine - origin.Y * sine,
					origin.X * sine + origin.Y * cosine
					);

				scene.circles[j] = circle;
			}
		}

		protected override void OnRenderFrame( FrameEventArgs e )
		{
			// called once per frame; render
			if( terminated )
			{
				Exit();
				return;
			}

			// cast 'dem rays!
			render.Trace(camera, scene, screen);

			// convert screen to OpenGL texture
			GL.BindTexture( TextureTarget.Texture2D, screenID );
			GL.TexImage2D( TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
						   screen.width, screen.height, 0,
						   OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
						   PixelType.UnsignedByte, screen.pixels
						 );

			// draw screen filling quad
			GL.Begin( PrimitiveType.Quads );
			GL.TexCoord2( 0.0f, 1.0f ); GL.Vertex2( -1.0f, -1.0f );
			GL.TexCoord2( 1.0f, 1.0f ); GL.Vertex2( 1.0f, -1.0f );
			GL.TexCoord2( 1.0f, 0.0f ); GL.Vertex2( 1.0f, 1.0f );
			GL.TexCoord2( 0.0f, 0.0f ); GL.Vertex2( -1.0f, 1.0f );
			GL.End();

			// tell OpenTK we're done rendering
			SwapBuffers();
		}

		public static void Main( string[] args )
		{
			// entry point
			using( OpenTKApp app = new OpenTKApp() ) { app.Run( 30.0, 0.0 ); }
		}
	}
}