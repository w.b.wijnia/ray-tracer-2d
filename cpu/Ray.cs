﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Template
{
    class Ray
    {
        public Vector2 origin;
        public Vector2 direction;
        public float t;

        public Ray(Vector2 origin, Vector2 direction, float t)
        {
            this.origin = origin;
            this.direction = direction;
            this.t = t;
        }

        // Determines whether this ray intersects the circle.
        public bool Intersect(Circle other)
        {
            // Step 0: Work everything out on paper!

            // Step 1: Gather all the relevant data.
            float ox = this.origin.X;
            float dx = this.direction.X;

            float oy = this.origin.Y;
            float dy = this.direction.Y;

            float x0 = other.origin.X;
            float y0 = other.origin.Y;
            float cr = other.radius;

            // Step 2: compute the substitutions.
            float p = ox - x0;
            float q = oy - y0;

            float r = 2 * p * dx;
            float s = 2 * q * dy;

            // Step 3: compute the substitutions, check if there is a collision.
            float a = dx * dx + dy * dy;
            float b = r + s;
            float c = p * p + q * q - cr * cr;

            float DSqrt = b * b - 4 * a * c;

            // no collision possible!
            if (DSqrt < 0)
            {
                return false;
            }

            // Step 4: compute the substitutions.
            float D = (float)Math.Sqrt(DSqrt);

            //float t0 = (-b + D) / (2 * a);
            float t1 = (-b - D) / (2 * a);

            if (t1 > 0 && t1 < this.t)
            {
                this.t = t1;
                return true;
            }

            return false;
        }
    }
}
