﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using OpenTK;

namespace Template
{
    class Render
    {
        // the number of threads that the parallel.for spawns
        private const int threadCount = 6;

        // the number of rays per pixel
        private const int rayCount = 1;

        public Render()
        {


        }

        // Traces the scene, determining visibility
        public void Trace(Camera camera, Scene scene, Surface screen)
        {
            // the pixels are independant on one another: compute them horizontally parallel!
            Parallel.For(0, screen.height, new ParallelOptions
            {
                MaxDegreeOfParallelism = threadCount
            }, (y) =>
            {
                // randomise it all!
                Random rng;
                if (true)
                {
                    // make sure that every thread has its own seed on the rng
                    long ticks = DateTime.Now.Ticks ^ (y + 1);
                    rng = new Random((int)ticks);
                }
                else
                {
                    // threads share the same seed (based on time), causing vertical 
                    // lines due to the same random choices being made. This is most
                    // visible when the rayCount equals 1.
                    rng = new Random();
                }

                for (int x = 0; x < screen.width; x++)
                {
                    // determine uv coordinates of the screen
                    float u = (float)x / screen.width;  // texture coordinate between {0, 1}
                    float v = (float)y / screen.height; // texture coordinate between {0, 1}

                    // the final color of the pixel 
                    Vector3 pixel = Vector3.Zero;
                    for (int r = 0; r < rayCount; r++)
                    {
                        // the center of the pixel in world coordinates
                        Vector2 center = camera.origin + new Vector2((u - 0.5f) * camera.dimensions.X, (v - 0.5f) * camera.dimensions.Y);

                        // the offset from that center
                        float ox = (float)(rng.NextDouble() - 0.5) * 2.0f;
                        float oy = (float)(rng.NextDouble() - 0.5) * 2.0f;
                        Vector2 offset = new Vector2(ox / camera.dimensions.X, oy / camera.dimensions.Y);

                        // the origin of the ray in world coordinates
                        Vector2 origin = center + offset;

                        // determine background color of the pixel
                        Vector3 background = Vector3.One;
                        foreach (Circle circle in scene.circles)
                            if (circle.IsInside(origin))
                                background = circle.color;

                        // determine light illumination of the pixel
                        Vector3 illumination = Vector3.Zero;
                        foreach (Light light in scene.lights)
                        {
                            // determine a point that is on the light, compute the direction and the distance
                            Vector2 pointOnPerimeter = light.circle.PointOnPerimeter(rng);
                            Vector2 direction = pointOnPerimeter - origin;
                            float distance = direction.Length;

                            // construct the ray we are interested in
                            Ray ray = new Ray(
                                origin: origin,
                                direction: direction.Normalized(),
                                t: distance
                                );

                            // determine visibility
                            bool occluded = false;
                            foreach (Circle circle in scene.circles)
                                occluded = occluded || ray.Intersect(circle);

                            // determine light attenuation
                            float attenuation = light.Attenuation(distance);
                            if (occluded)
                                attenuation = 0;

                            // add to the illumination. Take note: if occluded, attenuation is 0.
                            illumination += light.circle.color * attenuation * light.intensity;

                        }

                        // add it all up, sum accordingly to the number of rays
                        pixel += (1.0f / rayCount) * (background * illumination);
                    }

                    // plot it onto the screen
                    screen.Plot(x, y, ToRGB(pixel));
                }
            }
            );
        }

        // clamps the values of a vector3 between {0, 1} and changes it into some integer
        private int ToRGB(Vector3 color)
        {
            int r = (int)(255.0f * MathHelper.Clamp(color.X, 0.0f, 1.0f));
            int g = (int)(255.0f * MathHelper.Clamp(color.Y, 0.0f, 1.0f));
            int b = (int)(255.0f * MathHelper.Clamp(color.Z, 0.0f, 1.0f));

            return (r << 16) + (g << 8) + b;
        }
    }
}
