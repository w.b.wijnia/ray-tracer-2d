﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Template
{
    class Light
    {
        // the circle that represents this light. Becomes a 
        // point light when the radius is 0.
        public Circle circle;

        // The intensity of the light
        public float intensity;

        public Light(Circle circle, float intensity)
        {
            this.circle = circle;
            this.intensity = intensity;
        }

        public float Attenuation(float distance)
        {
            // return 1.0f / (distance * distance);
            return 1.0f / distance;
        }

    }
}
