﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Template
{
    class Scene
    {

        public List<Circle> circles;
        public List<Light> lights;

        public Scene()
        {
            lights = new List<Light>();
            circles = new List<Circle>();
        }

        public static Scene PresetA()
        {
            Scene scene = new Scene();

            // rng used for this preset
            Random rng = new Random(8);

            // more realistic colors
            Vector3[] colorsCircles =
            {
                new Vector3(1.0f, 1.0f, 1.0f),
                new Vector3(1.0f, 0.5f, 0.5f),
                new Vector3(0.5f, 1.0f, 1.0f),
                new Vector3(0.5f, 1.0f, 0.5f),
                new Vector3(1.0f, 0.5f, 1.0f),
                new Vector3(0.5f, 0.5f, 0.5f),
                new Vector3(1.0f, 0.0f, 0.0f),
            };

            Vector3[] colorsLights =
            {
                new Vector3(1.0f, 0.8f, 0.8f),
                new Vector3(0.8f, 1.0f, 1.0f),
                new Vector3(0.8f, 0.8f, 1.0f),
            };

            // debugging colors
            //Vector4[] colors =
            //{
            //    new Vector4(1.0f, 1.0f, 1.0f, 0),
            //    new Vector4(1.0f, 0.0f, 0.0f, 0),
            //    new Vector4(0.0f, 0.0f, 1.0f, 0),
            //    new Vector4(0.0f, 1.0f, 0.0f, 0),
            //};

            // generate a bunch of circles
            for (int j = 0; j < 5; j++)
            {
                float x = (float)rng.NextDouble() - 0.5f;
                float y = (float)rng.NextDouble() - 0.5f;
                float offset = 9 * ((float)rng.NextDouble() + 0.1f);
                Vector2 origin = offset * new Vector2(x, y).Normalized();
                float radius = 1 * ((float)rng.NextDouble() + 0.1f);
                Vector3 color = colorsCircles[rng.Next(0, colorsCircles.Length)];

                scene.circles.Add(new Circle(origin, radius, color));
            }

            // generate a bunch of lights
            for (int j = 0; j < 5; j++)
            {
                float x = (float)rng.NextDouble() - 0.5f;
                float y = (float)rng.NextDouble() - 0.5f;
                float offset = 9 * ((float)rng.NextDouble() + 0.1f);
                Vector2 origin = offset * new Vector2(x, y).Normalized();
                float radius = 0.1f * ((float)rng.NextDouble() + 0.1f);
                float intensity = 0.5f * ((float)rng.NextDouble() + 0.5f);
                Vector3 color = colorsLights[rng.Next(0, colorsLights.Length)];

                scene.lights.Add(new Light(new Circle(origin, radius, color), intensity));
            }

            return scene;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Scene\r\n");

            sb.Append("\tCircles\r\n");
            foreach (Circle circle in circles)
                sb.Append("\t\t" + circle.ToString() + "\r\n");

            sb.Append("\tLights\r\n");
            foreach (Light light in lights)
                sb.Append("\t\t" + light.ToString() + "\r\n");

            return sb.ToString();
        }

    }
}
