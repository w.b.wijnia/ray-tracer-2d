# Ray tracing in two dimensions

![](/images/preview.PNG)

## About this repository
This repository contains the source code for an old assignment of the Graphics course of the University of Utrecht. The repository contains a CPU-based and an OpenGL-based version of the ray tracer. Take note that a lot of the additional requirements are not implemented in the OpenGL-based version. This is for educational reasons: the aim is to understand how to make a ray tracer run on a fragment shader, not neccesarily how to run a super-complicated ray tracer faster.

The course and the template code is coordinated and made by Jacco Bikker.

## About the assignment
The assignment asks the students to construct a 2 dimensional ray tracer from the ground up. They've been provided with the template code because OpenGL itself hasn't been studied yet when the assignment starts. The goal of the assignment is:
 - Develop a basic ray tracer on the CPU, consisting of a few point lights and simple primitives.
And then additional requirements are:
 - Add separate world coordinate support (instead of screen coordinate support)
 - Add anti aliasing through shooting multiple rays on different locations within a pixel
 - Add soft shadows through area lights
 - Add texture sampling

And when you cannot get enough of it:
 - Develop a basic ray tracer on the GPU
 - Add normal mapping
 - Make a 3 dimensional-isch ray tracer that doesn't lose the 2 dimensional feel

And when you really cannot get enough of it:
 - Make a path tracer

## About the implementations
There are two versions:
 - A CPU version with the minimal requirements and some of the additional requirements
 - A GPU version with only the minimal requirements

Both versions run stable and out of the box. They are both based on OpenTK.

## Controls
You can move around in the scene by holding the left mouse button and then moving the mouse in both the CPU and GPU version.

## Tweaking
For the CPU version you can:
 - Adjust the resolution and offset in template.cs.
 - Adjust the thread count and ray count (per pixel) in renderer.cs.
For the GPU version you can:
 - Adjust the resolution in template.cs.

